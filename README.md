Flujo de trabajo y gestión de ramas
------------------------------------------------
Se utilizan las extensiones de Git Flow.

Configuración Apache 2.4 + libapache2-mod-php7.1
------------------------------------------------

    <VirtualHost *:443>
    
        ServerName front-demo-1.local
    
        ServerAdmin alvaro.delamata@grupo014.com
        DocumentRoot /home/alvaro/014media/front-demo-1/public
    
        DirectoryIndex index.php
    
        SSLEngine on
            SSLCertificateFile      /etc/apache2/certs/local.crt
            SSLCertificateKeyFile   /etc/apache2/certs/local.key
    
        <Directory /home/alvaro/014media/front-demo-1/public>
            AllowOverride None
            Require all granted
            <IfModule mod_rewrite.c>
                    Options -MultiViews
                    RewriteEngine On
                    RewriteCond %{REQUEST_FILENAME} !-f
                    RewriteRule ^(.*)$ index.php [QSA,L]
            </IfModule>
        </Directory>
    
    
        <Directory /var/www/project/public/bundles>
                <IfModule mod_rewrite.c>
                    RewriteEngine Off
                </IfModule>
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/front-demo-1.local.error.log
        CustomLog ${APACHE_LOG_DIR}/front-demo-1.local.access.log combined
    
    </VirtualHost>
    
    
    <VirtualHost *:80>
        ServerName front-demo-1.local
        Redirect permanent / https://front-demo-1.local/
    </VirtualHost>

